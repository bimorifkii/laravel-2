@extends('layout.master')
@section('judul')
    Halaman List Genre
@endsection
@section('isi')

    <form action="/film/{{ $film->id }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label for="title">Judul Film</label>
            <input type="text" class="form-control" name="judul" id="title" value='{{ $film->judul }}' placeholder="Masukkan Judul" >
            @error('judul')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="title">Ringkasan Film</label>
            <textarea name="ringkasan" class="form-control" id="ringkasan" cols="30" rows="10">{{ $film->ringkasan }}</textarea>
            @error('ringkasan')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="title">Tahun Rilis</label>
            <input type="text" class="form-control" name="tahun" id="tahun" value="{{ $film->tahun }}" placeholder="Masukkan tahun">
            @error('tahun')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="title">Genre Film</label>
            <select class="form-control" name="genre_id" id="genre_id">
                <option value="">- -Pilih Genre-- </option>
                @foreach ($genre as $item)
                    @if ($item->id === $film->genre_id)
                    <option value="{{ $item->id }}" selected>{{ $item->nama }}</option>
                    @else
                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                    @endif
                @endforeach
            </select>
            @error('genre_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="title">Poster Film</label></label>
            <input type="file" class="form-control" name="poster" id="poster" placeholder="Masukkan Title">
            @error('poster')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
@endsection
