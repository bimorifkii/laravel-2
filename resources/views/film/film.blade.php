@extends('layout.master')

@section('judul')
    Halaman Film
@endsection

@section('button')
@auth
<a href="/film/create" class="btn btn-primary my-2">Tambah Film</a>
@endauth
@endsection

@section('isi')
    <div class="row">
        @foreach ($film as $item)
            <div class="col-4">
                <div class="card">
                    <img class="card-img-top" src="{{ asset('poster/' . $item->poster) }}" alt="Card image cap" style="width: 300px; height: 400px">
                    <div class="card-body">
                        <h2 class="card-title">{{ $item->judul }} ({{ $item->tahun }})</h2>
                        <p class="card-text">{{ Str::limit($item->ringkasan, 50) }}</p>
                        <a href="/film/{{ $item->id }}" class="btn btn-primary">Detail</a>

                        @auth
                        <a href="/film/{{ $item->id }}/edit" class="btn btn-warning">Edit</a>
                        <form action="/film/{{ $item->id }}" method="POST" style="display:inline">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger my-1" value="Hapus" >
                        </form>
                        @endauth
                        
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection
