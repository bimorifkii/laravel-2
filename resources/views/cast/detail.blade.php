@extends('layout.master')

@section('judul')
    Halaman Detail Cast
@endsection

@section('isi')
    <table class="table">
        <thead class="thead-light">
            <tr></tr>
                <th style="width:33.3%; text-align: center">Nama</th>
                <th style="width:33.3%; text-align: center">Umur</th>
                <th style="width:33.3%; text-align: center">Bio</th>
            </tr>
        </thead>
        <tbody>
                <tr style="text-align: center">
                    <td>{{ $cast->nama }}</td>
                    <td>{{ $cast->umur }}</td>
                    <td>{{ $cast->bio }}</td>
                </tr>
        </tbody>
    </table>
@endsection
