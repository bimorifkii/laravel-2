@extends('layout.master')

@section('judul')
    Halaman Create Cast
@endsection

@section('isi')
    <form action="/cast" method="post">
        @csrf
        <div class="mb-3">
            <label for="Nama" class="form-label">Nama</label>
            <input type="text" class="form-control" id="Nama" name="nama">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="Umur" class="form-label">Umur</label></label>
            <textarea class="form-control" id="Umur" rows="3" name="umur"></textarea>
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="mb-3">
            <label for="Bio" class="form-label">Bio</label></label>
            <textarea class="form-control" id="Bio" rows="3" name="bio"></textarea>
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button class="btn btn-primary" type="submit">Tambah Data</button>
    </form>
@endsection
