<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use app\Http\Controllers\CastControllers;



Route::group(['middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index');

    // Route::get('/cast', 'CastControllers@index');
    // Route::get('/cast/create', 'CastControllers@create');
    // Route::post('/cast', 'CastControllers@store');
    // Route::get('/cast/{cast_id}', 'CastControllers@show');
    // Route::get('/cast/{cast_id}/edit', 'CastControllers@edit');
    // Route::put('/cast/{cast_id}', 'CastControllers@update');
    // Route::delete('/cast/{cast_id}', 'CastControllers@destroy');
    
    // Route::get('/film', 'FilmController@index');
    // Route::get('/film/create', 'FilmController@create');
    // Route::post('/film', 'FilmController@store');
    // Route::get('/film/{film_id}', 'FilmController@show');
    // Route::get('/film/{film_id}/edit', 'FilmController@edit');
    // Route::put('/film/{film_id}', 'FilmController@update');
    // Route::delete('/film/{film_id}', 'FilmController@destroy');
});
Route::resource('film', 'FilmController');
Route::resource('cast', 'CastControllers');
Auth::routes();
