<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Film;
use App\Genre;
use File;

class FilmController extends Controller
{
    public function __construct()
    {

        $this->middleware('auth')->except('index', 'show');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $film = Film::all();
        return view('film.film', compact('film'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $genre = Genre::all();
        return view('film.create', compact('genre'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request,[
        //     'judul' => 'required',
        //     'ringkasan' => 'required',
        //     'tahun' => 'required',
        //     'poster' => 'required | mimes: jpg,jpeg,png | max: 2200',
        //     'genre_id' => 'required'
        // ]);
        // $poster = $request->poster;
        // $nama_poster = time() . '-' . $poster->getClientOriginalName();

        // Film::create([
        //     'judul' => $request->judul,
        //     'ringkasan' => $request->ringkasan,
        //     'tahun' => $request->tahun,
        //     'poster' =>$nama_poster,
        //     'genre_id' => $request->genre_id
        // ]);
        // $poster->move('poster/' , $nama_poster);
        // return redirect('/film');

        $this->validate($request,[
    		'judul' => 'required',
    		'ringkasan' => 'required',
            'poster' => 'mimes: jpeg,jpg,png|max:1000000',
            'genre_id' => 'required',
            'tahun' => 'required',
    	]);
        if ($request->has('poster')){
        $poster = $request->poster;
        $a = time() . "-" . $poster->getClientOriginalName(); 
        $film=[
    		'judul' => $request->judul,
    		'ringkasan' => $request->ringkasan,
            'poster' => $a,
            'genre_id' => $request->genre_id,
            'tahun' => $request->tahun
    	];
        $poster->move('poster/',$a);
    }else{
       $film=[
    		'judul' => $request->judul,
    		'ringkasan' => $request->ringkasan,
            'genre_id' => $request->genre_id,
            'tahun' => $request->tahun
        ];
    }
        Film::create($film);
    	return redirect('/film');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $film = Film::find($id);
        return view('film.detail', compact('film'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $film = Film::find($id);
        $genre = Genre::all();
        $idselect = Film::find($id);
        return view('film.edit', compact('film', 'genre', 'idselect'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
    		'judul' => 'required',
    		'ringkasan' => 'required',
            'poster' => 'mimes: jpeg,jpg,png|max:2200',
            'genre_id' => 'required',
            'tahun' => 'required',
    	]);
        $film = Film::findorfail($id);

        if ($request->has('poster')){
            //post->delete();
            $path ="poster/";
            File::delete($path . $film->poster);
            $poster =$request->poster;
            $new_poster = time() . ' - ' .$poster->getClientOriginalName();
            $poster->move('poster/',$new_poster);
            $film_data = [
                'judul' => $request->judul,
                'ringkasan' => $request->ringkasan,
                'poster' => $new_poster,
                'genre_id' => $request->genre_id,
                'tahun' => $request->tahun
            ];
        }else {
            $film_data =[
                'judul' => $request->judul,
                'ringkasan' => $request->ringkasan,
                'tahun' => $request->tahun,
                'poster' == '',
                'genre_id' => $request->genre_id
            ];
        }
        $film->update($film_data);

        return redirect('/film');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $film = Film::find($id)->delete();
        return redirect('film');
    }
}
