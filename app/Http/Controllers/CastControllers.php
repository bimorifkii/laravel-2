<?php

namespace App\Http\Controllers;
use DB;
use App\CastModel;
use Illuminate\Http\Request;

class CastControllers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cast = CastModel::all();
        return view('cast.cast', compact('cast'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cast.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama'=>'required',
            'umur'=>'required',
            'bio'=>'required'
        ]);

        CastModel::create([
            'nama' => $request->nama,
            'umur' => $request->umur,
            'bio' => $request->bio
        ]);

        return redirect('cast');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cast = CastModel::find($id);
        return view('cast.detail', compact('cast'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cast = CastModel::find($id);
        return view('cast.edit', compact('cast'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama'=>'required',
            'umur'=>'required',
            'bio'=>'required'
        ]);

        $cast = CastModel::find($id);
        $cast->update([
            'nama'=>$request->nama,
            'umur'=>$request->umur,
            'bio'=>$request->bio,
        ]);

        return redirect('cast');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $query = CastModel::find($id)->delete();
        return redirect('/cast');
    }
}
